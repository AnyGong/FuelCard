var baseDpr = 3;
module.exports = {
    px2rem: {
        baseDpr: baseDpr,             // base device pixel ratio (default: 2)
        threeVersion: false,    // whether to generate @1x, @2x and @3x version (default: false)
        remVersion: true,       // whether to generate rem version (default: true)
        remUnit: 1080,            // rem unit value (default: 75)
        remPrecision: 6         // rem precision (default: 6)
    },
    autoprefixer: {},
    minifyCss: {
        advanced: false,//类型：Boolean 默认：true [是否开启高级优化（合并选择器等）]
        compatibility: 'ie7',//保留ie7及以下兼容写法 类型：String 默认：''or'*' [启用兼容模式； 'ie7'：IE7兼容模式，'ie8'：IE8兼容模式，'*'：IE9+兼容模式]
        keepBreaks: false//类型：Boolean 默认：false [是否保留换行]
    },
    base64: {
        extensions: ['png', 'ttf'],
        maxImageSize: 100000, // bytes
    },
    htmlOne: {
        removeSelector: '', // selector 配置，源文档中符合这个selector的标签将自动移除
        keepliveSelector: 'script',  // 不要combo的selector配置，源文档中符合这个selector的将保持原状。如果是相对路径，则会根据配置修改成线上可访问地址
        destDir: './',  // dest 目录配置，一般情况下需要和 gulp.dest 目录保持一致，用于修正相对路径资源
        coimport: true,  // 是否需要把 @import 的资源合并
        cssminify: true, // 是否需要压缩css
        jsminify: true,  // 是否需要压缩js
        publish: false, // 是否是发布到线上的文件，false为发布到日常，预发理论上和日常的文件内容一样
        appName: 'app-xxx', //仓库名
        appVersion: '0.0.0', //发布版本号
        noAlicdn: false, // js相对路径拼接的域名是否是alicdn的
        onlinePath: '', //非alicdn线上域名
        dailyPath: '', //非alicdn日常域名
    }
}