module.exports = {
    now: new Date(),
    year: new Date().getFullYear(),
    month: (new Date().getMonth() + 1) < 10 ? "0" + (new Date().getMonth() + 1) : (new Date().getMonth() + 1),
    day: new Date().getDate() < 10 ? "0" + new Date().getDate() : new Date().getDate(),
    week: new Date().getDay() < 10 ? "0" + new Date().getDay() : new Date().getDay(),
    hours: new Date().getHours() < 10 ? "0" + new Date().getHours() : new Date().getHours(),
    minutes: new Date().getMinutes() < 10 ? "0" + new Date().getMinutes() : new Date().getMinutes(),
    seconds: new Date().getSeconds() < 10 ? "0" + new Date().getSeconds() : new Date().getSeconds(),
    dateFormat: function () {
        return this.year.toString() + "_" + this.month.toString() + this.day.toString() + "_" + this.week + "_" + this.hours.toString() + this.minutes.toString() + this.seconds.toString()
    },
    dateFormatContinuous: function () {
        return this.year.toString() + this.month.toString() + this.day.toString() + this.week + this.hours.toString() + this.minutes.toString() + this.seconds.toString()
    }
}