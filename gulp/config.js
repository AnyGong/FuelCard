/* gulp命令会由gulpfile.js运行，所以src和build文件夹路径如下（根目录下） */
var src = './src';
var dest = src;
var dist = './dist';
var build = './build';
var assets = './assets';
var released = './released';
var repository = './repository';

module.exports = {
    root: {
        src: src,
        assets: assets,
        dest: dest,
        repository: repository,
        dist: {
            root: dist,
            css: dist + '/css/',
            script: dist + '/script/',
            scriptLibrary: dist + '/library/',
            scriptPlugins: dist + '/plugins/',
            scriptComponent: dist + '/components/',
            image: dist + '/image/',
            html: dist + '/'
        },
        released: released,
        build: build
    },
    scss: {
        src: src + "/scss/**/*.scss",	    //需要编译的scss
        dest: dest + "/css/",	            //输出目录
        settings: {					    //编译scss过程需要的配置，可以为空

        }
    },
    script: {
        src: src + "/script/**/*.js",	    //需要编译s的scss
        scriptLibrary: src + '/library/**.js',  //需要编译的script library
        scriptPlugins: src + '/plugins/**.js',  //需要编译的script library
        scriptComponent: src + '/components/**.js',  //需要编译的script  component
        dest: dest + "/script/",	            //输出目录
    },
    html: {
        src: src + "/**/*.html",
        dest: dest + "/",
    },
    image: {
        src: src + "/image/*.{jpeg,jpg,png,gif}",
        dest: dest + "/image/",
    }
}