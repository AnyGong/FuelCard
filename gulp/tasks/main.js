var fs = require('fs');
var path = require('path');
var gulp = require('gulp');
var del = require('del');
var uglify = require('gulp-uglify');
var md5 = require('md5');
var htmlmin = require('gulp-htmlmin');
var minifyCss = require('gulp-clean-css');
var header = require('gulp-header');
var time = require('../util/time');
var config = require('../config');
var plugin = require('../plugin');
var pkg = require('../../package');
var root = config.root;
var runSequence = require('run-sequence');
var timeVal = time.dateFormat();
var appVersion = pkg.name + '_' + pkg.version;
var versionFormat = appVersion + '_' + timeVal;
var headBanner = '/**! ' + versionFormat + '_' + pkg.author + '*/\n';
var banner = versionFormat + '_' + pkg.author;
var headHtmlBanner = '<!--' + banner + '-->\n';
var archiveFile = path.resolve(root.released, versionFormat + '.zip');
var repositoryFile = root.repository + "/" + versionFormat + '/';


/**
 * 发布新版本时，保存历史源代码
 */
gulp.task('repository', function () {
    return gulp.src([
        root.src + '/**/*'
    ], {
        dot: true
    }).pipe(gulp.dest(repositoryFile))
});


/**
 * 生成Android Studio 调试版本,不包含 icon 和 launch
 */
gulp.task('build', ['scss'], function () {
    return gulp.src([
        root.dest + '/**/*',
        '!' + root.dest + '/icon/',
        '!' + root.dest + '/icon/*',
        '!' + root.dest + '/icon/**/*',
        '!' + root.dest + '/launch/',
        '!' + root.dest + '/launch/*',
        '!' + root.dest + '/launch/**/*',
        '!' + root.dest + '/scss/',
        '!' + root.dest + '/scss/*',
        '!' + root.dest + '/scss/**/*'
    ], {
        dot: true
    }).pipe(gulp.dest(root.build))
});


/**
 * 发布版本时，保留未压缩版本
 */
gulp.task('assets', function () {
    return gulp.src([
        root.dest + '/**/*',
        '!' + root.dest + '/icon/',
        '!' + root.dest + '/icon/*',
        '!' + root.dest + '/icon/**/*',
        '!' + root.dest + '/launch/',
        '!' + root.dest + '/launch/*',
        '!' + root.dest + '/launch/**/*',
        '!' + root.dest + '/scss/',
        '!' + root.dest + '/scss/*',
        '!' + root.dest + '/scss/**/*'
    ], {
        dot: true
    }).pipe(gulp.dest(root.assets + '/' + versionFormat))
});


/**
 * 压缩  css/script/html/misc
 */
gulp.task('uglify:html', function () {
    gulp.src(config.html.src)
        .pipe(htmlmin({
            collapseWhitespace: true,
            minifyJS: true,
            minifyCSS: true
        }))
        .pipe(header(headHtmlBanner))
        .pipe(gulp.dest(root.dist.html))
});
gulp.task('uglify:css', ['scss'], function () {
    gulp.src(config.scss.dest + '/**/*.css')
        .pipe(minifyCss(plugin.minifyCss))
        .pipe(header(headBanner))
        .pipe(gulp.dest(root.dist.css))
});
gulp.task('uglify:image', function () {
    gulp.src(config.image.src)
        .pipe(gulp.dest(root.dist.image))
});
gulp.task('uglify:script', function () {
    gulp.src(config.script.src)
        .pipe(uglify())
        .pipe(header(headBanner))
        .pipe(gulp.dest(root.dist.script));

    gulp.src(config.script.scriptLibrary)
        .pipe(uglify())
        .pipe(header(headBanner))
        .pipe(gulp.dest(root.dist.scriptLibrary));

    gulp.src(config.script.scriptPlugins)
        .pipe(uglify())
        .pipe(header(headBanner))
        .pipe(gulp.dest(root.dist.scriptPlugins));

    gulp.src(config.script.scriptComponent)
        .pipe(uglify())
        .pipe(header(headBanner))
        .pipe(gulp.dest(root.dist.scriptComponent));
});
gulp.task('uglify:misc', function () {
    gulp.src(root.dest + '/config.xml')
        .pipe(gulp.dest(root.dist.root + '/'))
});


/**
 * uglify concat
 */
gulp.task('uglify', ['uglify:html', 'uglify:css', 'uglify:image', 'uglify:script', 'uglify:misc']);


/**
 * dist 压缩到 APICloud测试的版本
 */
gulp.task('dist', function (done) {
    runSequence(
        ['clean:dist'],
        'uglify',
        done)
});


/**
 * 发布到正式的APK包中
 */
gulp.task('released:version', function () {
    var version = path.resolve(root.dist.root + '/', versionFormat + '.md')
    fs.createWriteStream(version);
});
gulp.task('released', function (done) {
    runSequence(
        ['dist', 'archive:create_archive_dir'],
        'repository',
        'archive:zip',
        'assets',
        done
    )
});


/**
 * 压缩发布的文件
 */
gulp.task('archive:create_archive_dir', function () {
    fs.exists(root.released, function (exists) {
        if (!exists) {
            fs.mkdirSync(path.resolve(root.released), '0755');
        }
    });
});
gulp.task('archive:zip', ['released:version'], function (done) {
    setTimeout(function () {
        var archiver = require('archiver')('zip');
        var files = require('glob').sync('**/*.*', {
            'cwd': root.dist.root + '/',
            'dot': true
        });
        var output = fs.createWriteStream(archiveFile);

        archiver.on('error', function (error) {
            done();
            throw error;
        });

        output.on('close', done);

        files.forEach(function (file) {

            var filePath = path.resolve(root.dist.root + '/', file);

            archiver.append(fs.createReadStream(filePath), {
                'name': file,
                'mode': fs.statSync(filePath)
            });

        });

        archiver.pipe(output);
        archiver.finalize();
    }, 3000)
});