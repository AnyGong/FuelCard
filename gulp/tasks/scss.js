var gulp = require('gulp');
var sass = require('gulp-sass');
var px2rem = require('gulp-px3rem');
var rename=require('gulp-rename');
var handleErrors = require('../util/handleErrors');
var autoprefixer = require('gulp-autoprefixer');
var base64 = require('gulp-base64');
var config = require('../config').scss;
var plugin = require('../plugin');

gulp.task('scss', function () {
    gulp.src(config.src)
        .pipe(sass(config.settings))
        .on('error', handleErrors)
        .pipe(px2rem(plugin.px2rem))
        .pipe(autoprefixer(plugin.autoprefixer))
        .pipe(base64(plugin.base64))
        .pipe(rename(plugin.base64))
        .pipe(gulp.dest(config.dest))
});