var gulp = require('gulp');
var del = require('del');
var root = require('../config').root;
/**
 * clear 清除文件夹任务
 * */
// 清除dist
gulp.task('clean:dist', function () {
    return del([root.dist.root]);
});
// 清除build
gulp.task('clean:build', function () {
    return  del([root.build]);
});
