/**
 * 基本头部
 */
Vue.component('head-view', {
    template: '<div id="head" :class="head">' +
    '<div class="head-bd">' +
    '<slot name="head">' +
    '<slot name="left">' +
    '<div tapmode="active-state" v-show="backIcon" @click="keyBack()" class="head-left back-arrow"></div>' +
    '</slot>' +
    '<slot name="title">' +
    '<p v-text="title" class="head-title"></p>' +
    '</slot>' +
    '<slot name="right">' + '</slot>' +
    '</slot>' +
    '</div>' +
    '<slot name="extend">' + '</slot>' +
    '</div>',
    data: function () {
        return {
            head: 'head ' + HEAD.class
        }
    },
    methods: {
        keyBack: function () {
            if (!this.backIcon) return;
            fl.triggerKeyBack();
        }
    },
    props: {
        backIcon: {
            default: true
        },
        title: {
            default: HEAD.title
        }
    }
});

/**
 * ======================================================每个项目特色配置
 */

