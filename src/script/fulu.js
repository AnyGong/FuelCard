/*
 * Fulu Network Technology Co., Ltd.
 * */
!function (factory) {
    if (typeof require === 'function' && typeof exports === 'object' && typeof module === 'object') {
        var target = module['exports'] || exports;
        factory(target);
    } else if (typeof define === 'function' && (define.amd || define.cmd)) {
        define(['exports'], factory);
    } else {
        factory(window['fl'] = {});
    }
}(function ($) {
    /**
     * noop
     */
    $.noop = function () {
    };

    /**
     * FastClick
     * @link https://github.com/ftlabs/fastclick
     */
    if ('addEventListener' in document) {
        document.addEventListener('DOMContentLoaded', function () {
            FastClick.attach(document.body);
        }, false);
    }

    /**
     * global init Variables
     */
    var storage = $api.getStorage('storage') || {
            mkey: '',
            mid: '',
            account: '',
            nickName: ''
        };
    // 测试专线
    //=====================================================
    $.mkey = RELEASED ? storage.mkey : '2e55cc13b522423aac131a1ef712ce9f'; // mkey
    $.mid = RELEASED ? storage.mid : '100000496'; // mid
    //=====================================================
    $.account = storage.account; // account
    $.nickName = storage.nickName; // nickName
    //====================================================

    // log 标识 打印日志专用标识
    $.log = '===>';
    // frame窗口返回键标识
    $.keyBackFrame = false;
    //win窗口返回键标识
    $.keyBackWin = false;
    // 网络连接状态
    $.connectionType = true;
    // ios 适配侵入式效果， 并改变head高度
    var winHeadHeight = device.ios ? HEAD.heightContainStatus.android.ios : HEAD.height;
    // ios 适配侵入式效果
    $.barStatusClass = device.ios ? 'ios-status' : '';
    //计算head的高度，设置打开的frame y 设定。根据比例缩小
    $.rectY = getThreeVersion(winHeadHeight);
    // 计算出头部的高度,主要适用于在css中
    $.winHeadHeight = winHeadHeight;
    // 当前tost提示文本,防止页面过渡的不断弹出提示信息
    $.toastTxt = '';

    return fl
});


/**
 * Vue 常用的
 */
(function (Vue, $, window) {

    /**
     * Directive============================================
     */
    /**
     * $.openWin
     */
    Vue.directive('open-win', {
        deep: true,
        update: function (option) {
            this.el.addEventListener('click', function () {
                $.openWin(option);
            }, false)
        }
    });

    /**
     * $.openFrame
     */
    Vue.directive('open-frame', {
        deep: true,
        update: function (option) {
            this.el.addEventListener('click', function () {
                $.openFrame(option);
            }, false)
        }
    });

    /**
     * $.closeWin
     */
    Vue.directive('close-win', {
        deep: true,
        update: function (option) {
            this.el.addEventListener('click', function () {
                $.closeWin(option);
            }, false)
        }
    });

    /**
     * $.closeFrame
     */
    Vue.directive('close-frame', {
        deep: true,
        update: function (option) {
            this.el.addEventListener('click', function () {
                $.closeFrame(option);
            }, false)
        }
    });

    /**
     * 响应按钮返回事件
     */
    Vue.directive('trigger-back', function () {
        this.el.addEventListener('click', function () {
            $.triggerKeyBack();
        }, false)
    });

    /**
     * 当指定的对象更新时，且下面有属于 tap-mode的话，就会调用一次 api.parseTapmode
     */
    Vue.directive('tap-mode', {
        deep: true,
        update: function () {
            window.api && api.parseTapmode();
        }
    });

    /**
     * 图片缓存加载，采取 APICloud的图片缓存策略
     * 如果 ret.status 为 false 的话，就请求网络地址，请求10s之后如还是没有图片，将采取默认的图片加载
     */
    Vue.directive('cache-img', {
        update: function (url) {
            var $el = this.el;
            if (url) {
                window.api && api.imageCache({
                    url: url
                }, function (ret, err) {
                    if (ret.status) {
                        $el.setAttribute('src', ret.url);
                    }
                    if (!ret.status) {
                        var img = new Image();
                        img.src = value;
                        function imgLoad(callback) {
                            var time = 0;
                            var timer = setInterval(function () {
                                if (img.complete) {
                                    callback(img.src);
                                    clearInterval(timer);
                                }
                                time += 1;
                                if (time == 10) {
                                    callback($el.getAttribute('src'));
                                    clearInterval(timer);
                                }
                            }, 1000)
                        }

                        imgLoad(function (path) {
                            $el.setAttribute('src', path);
                        })
                    }
                });
            }
        }
    });

    /**
     * 退出应用
     */
    Vue.directive('close-widget', {
        bind: function () {
            this.el.addEventListener('click', function () {
                $.closeWidget();
            }, false)
        }
    });

    /**
     * 关闭frameModal窗口 $.closeFrameModal
     */
    Vue.directive('close-frame-modal', {
        bind: function () {
            this.el.addEventListener('click', function () {
                $.closeFrameModal();
            }, false)
        }
    });

    /**
     * 清除文本框输入，主要用到文本框的后面的 del 对象
     */
    Vue.directive('clear-input', {
        bind: function () {
            var $el = this.el;
            var $JClearInput = $el.querySelector('.j-clear-input');
            var $JModelInput = $el.querySelector('.j-model-input');

            function setClearStatus() {
                if ($JModelInput.value.length) return $JClearInput.style.visibility = 'visible';
                $JClearInput.style.visibility = 'hidden';
            }

            $JModelInput.addEventListener('click', function () {
                setClearStatus();
            }, false);
            $JModelInput.addEventListener('input', function () {
                setClearStatus();
            }, false);
            $JClearInput.addEventListener('click', function () {
                $JModelInput.value = '';
                this.style.visibility = 'hidden';
                if (device.ios) {
                    setTimeout(function () {
                        $JModelInput.focus();
                    }, 200);
                    return
                }
                $JModelInput.focus();
            }, false)
        }
    });


    /**
     * components============================================
     */
    /**
     * 页面loading
     * NOTE!! 页面调用的该组建后,一定要调用 frameLoaded 方法，页面才会被显示
     *
     */
    Vue.component('page-view', {
        template: '<div v-el:loading class="page-loading" v-show="loading">' +
        '<div v-show="!network" class="ui-network-error">' +
        '<div class="i-network-error"></div>' +
        '<p class="network-error-title">未连接到网络</p>' +
        '<div @click="refreshNetwork()" class="refresh-btn">轻触刷新</div>' +
        '</div>' +
        '</div>' +
        '<slot></slot>',
        data: function () {
            return {
                network: true,
                loading: true
            }
        },
        watch: {
            network: function (val) {
                if (!val) {
                    $.toast($.setting.netWorkToast);
                }
            }
        },
        methods: {
            refreshNetwork: function () {
                if ($.getNetwork()) {
                    this.network = true;
                    this.$nextTick(function () {
                        api.execScript(_.assign({
                            script: 'apiready()'
                        }, api.frameName ? {
                            frameName: api.frameName
                        } : {
                            name: api.winName
                        }));
                    });
                    return;
                }
                $.toast($.setting.netWorkToast);
            }
        }
    });


    /**
     * mxins============================================
     */
    /**
     * 窗口方法
     */
    Vue.mixin({
        methods: {
            openWin: function () {
                $.openWin(Array.prototype.slice.call(arguments));
            },
            openFrame: function () {
                $.openFrame(Array.prototype.slice.call(arguments));
            },
            openFrameInWin: function () {
                $.openFrameInWin(Array.prototype.slice.call(arguments));
            }
        }
    });


    /**
     * transition============================================
     */
    /**
     * fade
     */
    /**
     * 渐变动画
     */
    Vue.transition('fade', {
        enterClass: 'fadeIn',
        leaveClass: 'fadeOut'
    })


})(Vue, fl, window);


/**
 * md5加密 && base64 加密/解密
 */
(function ($) {
    /**
     * base64
     */
    $.base64 = (function () {
        var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        var base64DecodeChars = new Array(
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
            -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63,
            52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1,
            -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
            15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1,
            -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
            41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1);
        var base64 = {};
        base64.encode = function (str) {
            var out, i, len;
            var c1, c2, c3;

            len = str.length;
            i = 0;
            out = "";
            while (i < len) {
                c1 = str.charCodeAt(i++) & 0xff;
                if (i == len) {
                    out += base64EncodeChars.charAt(c1 >> 2);
                    out += base64EncodeChars.charAt((c1 & 0x3) << 4);
                    out += "==";
                    break;
                }
                c2 = str.charCodeAt(i++);
                if (i == len) {
                    out += base64EncodeChars.charAt(c1 >> 2);
                    out += base64EncodeChars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
                    out += base64EncodeChars.charAt((c2 & 0xF) << 2);
                    out += "=";
                    break;
                }
                c3 = str.charCodeAt(i++);
                out += base64EncodeChars.charAt(c1 >> 2);
                out += base64EncodeChars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
                out += base64EncodeChars.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
                out += base64EncodeChars.charAt(c3 & 0x3F);
            }
            return out;
        };
        base64.decode = function (str) {
            var c1, c2, c3, c4;
            var i, len, out;

            len = str.length;
            i = 0;
            out = "";
            while (i < len) {
                /* c1 */
                do {
                    c1 = base64DecodeChars[str.charCodeAt(i++) & 0xff];
                } while (i < len && c1 == -1);
                if (c1 == -1)
                    break;

                /* c2 */
                do {
                    c2 = base64DecodeChars[str.charCodeAt(i++) & 0xff];
                } while (i < len && c2 == -1);
                if (c2 == -1)
                    break;

                out += String.fromCharCode((c1 << 2) | ((c2 & 0x30) >> 4));

                /* c3 */
                do {
                    c3 = str.charCodeAt(i++) & 0xff;
                    if (c3 == 61)
                        return out;
                    c3 = base64DecodeChars[c3];
                } while (i < len && c3 == -1);
                if (c3 == -1)
                    break;

                out += String.fromCharCode(((c2 & 0XF) << 4) | ((c3 & 0x3C) >> 2));

                /* c4 */
                do {
                    c4 = str.charCodeAt(i++) & 0xff;
                    if (c4 == 61)
                        return out;
                    c4 = base64DecodeChars[c4];
                } while (i < len && c4 == -1);
                if (c4 == -1)
                    break;
                out += String.fromCharCode(((c3 & 0x03) << 6) | c4);
            }
            return out;
        };
        base64.utf16to8 = function (str) {
            var out, i, len, c;

            out = "";
            len = str.length;
            for (i = 0; i < len; i++) {
                c = str.charCodeAt(i);
                if ((c >= 0x0001) && (c <= 0x007F)) {
                    out += str.charAt(i);
                } else if (c > 0x07FF) {
                    out += String.fromCharCode(0xE0 | ((c >> 12) & 0x0F));
                    out += String.fromCharCode(0x80 | ((c >> 6) & 0x3F));
                    out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));
                } else {
                    out += String.fromCharCode(0xC0 | ((c >> 6) & 0x1F));
                    out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));
                }
            }
            return out;
        };
        base64.utf8to16 = function (str) {
            var out, i, len, c;
            var char2, char3;

            out = "";
            len = str.length;
            i = 0;
            while (i < len) {
                c = str.charCodeAt(i++);
                switch (c >> 4) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        // 0xxxxxxx
                        out += str.charAt(i - 1);
                        break;
                    case 12:
                    case 13:
                        // 110x xxxx   10xx xxxx
                        char2 = str.charCodeAt(i++);
                        out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
                        break;
                    case 14:
                        // 1110 xxxx  10xx xxxx  10xx xxxx
                        char2 = str.charCodeAt(i++);
                        char3 = str.charCodeAt(i++);
                        out += String.fromCharCode(((c & 0x0F) << 12) |
                            ((char2 & 0x3F) << 6) |
                            ((char3 & 0x3F) << 0));
                        break;
                }
            }

            return out;
        };
        base64.CharToHex = function (str) {
            var out, i, h;
            out = "";
            i = 0;
            while (i < str.length) {
                h = str.charCodeAt(i++).toString(16);
                out += "\\0x" + h;
                out += (i > 0 && i % 8 == 0) ? "\r\n" : ", ";
            }
            return out;
        };
        base64.doEncode = function (str) {
            return base64.encode(base64.utf16to8(str));
        };
        base64.doDecode = function (str) {
            return base64.utf8to16(base64.decode(str));
        };

        return base64;
    })();

    /**
     * md5
     */
    $.md5 = (function () {
        var md5 = {};
        md5.doEncode = function (data) {
            // convert number to (unsigned) 32 bit hex, zero filled string
            function to_zerofilled_hex(n) {
                var t1 = (n >>> 0).toString(16)
                return "00000000".substr(0, 8 - t1.length) + t1
            }

            // convert array of chars to array of bytes
            function chars_to_bytes(ac) {
                var retval = []
                for (var i = 0; i < ac.length; i++) {
                    retval = retval.concat(str_to_bytes(ac[i]))
                }
                return retval
            }

            // convert a 64 bit unsigned number to array of bytes. Little endian
            function int64_to_bytes(num) {
                var retval = []
                for (var i = 0; i < 8; i++) {
                    retval.push(num & 0xFF)
                    num = num >>> 8
                }
                return retval
            }

            // 32 bit left-rotation
            function rol(num, places) {
                return ((num << places) & 0xFFFFFFFF) | (num >>> (32 - places))
            }

            // The 4 MD5 functions
            function fF(b, c, d) {
                return (b & c) | (~b & d)
            }

            function fG(b, c, d) {
                return (d & b) | (~d & c)
            }

            function fH(b, c, d) {
                return b ^ c ^ d
            }

            function fI(b, c, d) {
                return c ^ (b | ~d)
            }

            // pick 4 bytes at specified offset. Little-endian is assumed
            function bytes_to_int32(arr, off) {
                return (arr[off + 3] << 24) | (arr[off + 2] << 16)
                    | (arr[off + 1] << 8) | (arr[off])
            }

            /*
             * Conver string to array of bytes in UTF-8 encoding See:
             * http://www.dangrossman.info/2007/05/25/handling-utf-8-in-javascript-php-and-non-utf8-databases/
             * http://stackoverflow.com/questions/1240408/reading-bytes-from-a-javascript-string
             * How about a String.getBytes(<ENCODING>) for Javascript!? Isn't it time
             * to add it?
             */
            function str_to_bytes(str) {
                var retval = []
                for (var i = 0; i < str.length; i++)
                    if (str.charCodeAt(i) <= 0x7F) {
                        retval.push(str.charCodeAt(i))
                    } else {
                        var tmp = encodeURIComponent(str.charAt(i)).substr(1)
                            .split('%')
                        for (var j = 0; j < tmp.length; j++) {
                            retval.push(parseInt(tmp[j], 0x10))
                        }
                    }
                return retval
            }

            // convert the 4 32-bit buffers to a 128 bit hex string. (Little-endian is
            // assumed)
            function int128le_to_hex(a, b, c, d) {
                var ra = ""
                var t = 0
                var ta = 0
                for (var i = 3; i >= 0; i--) {
                    ta = arguments[i]
                    t = (ta & 0xFF)
                    ta = ta >>> 8
                    t = t << 8
                    t = t | (ta & 0xFF)
                    ta = ta >>> 8
                    t = t << 8
                    t = t | (ta & 0xFF)
                    ta = ta >>> 8
                    t = t << 8
                    t = t | ta
                    ra = ra + to_zerofilled_hex(t)
                }
                return ra
            }

            // conversion from typed byte array to plain javascript array
            function typed_to_plain(tarr) {
                var retval = new Array(tarr.length)
                for (var i = 0; i < tarr.length; i++) {
                    retval[i] = tarr[i]
                }
                return retval
            }

            // check input data type and perform conversions if needed
            var databytes = null;
            // String
            var type_mismatch = null;
            if (typeof data == 'string') {
                // convert string to array bytes
                databytes = str_to_bytes(data)
            } else if (data.constructor == Array) {
                if (data.length === 0) {
                    // if it's empty, just assume array of bytes
                    databytes = data
                } else if (typeof data[0] == 'string') {
                    databytes = chars_to_bytes(data)
                } else if (typeof data[0] == 'number') {
                    databytes = data
                } else {
                    type_mismatch = typeof data[0]
                }
            } else if (typeof ArrayBuffer != 'undefined') {
                if (data instanceof ArrayBuffer) {
                    databytes = typed_to_plain(new Uint8Array(data))
                } else if ((data instanceof Uint8Array) || (data instanceof Int8Array)) {
                    databytes = typed_to_plain(data)
                } else if ((data instanceof Uint32Array)
                    || (data instanceof Int32Array)
                    || (data instanceof Uint16Array)
                    || (data instanceof Int16Array)
                    || (data instanceof Float32Array)
                    || (data instanceof Float64Array)) {
                    databytes = typed_to_plain(new Uint8Array(data.buffer))
                } else {
                    type_mismatch = typeof data
                }
            } else {
                type_mismatch = typeof data
            }

            if (type_mismatch) {
                console.error($.log + 'MD5 type mismatch, cannot process ' + type_mismatch)
            }

            function _add(n1, n2) {
                return 0x0FFFFFFFF & (n1 + n2)
            }

            return do_digest()

            function do_digest() {

                // function update partial state for each run
                function updateRun(nf, sin32, dw32, b32) {
                    var temp = d
                    d = c
                    c = b
                    // b = b + rol(a + (nf + (sin32 + dw32)), b32)
                    b = _add(b, rol(_add(a, _add(nf, _add(sin32, dw32))), b32))
                    a = temp
                }

                // save original length
                var org_len = databytes.length

                // first append the "1" + 7x "0"
                databytes.push(0x80)

                // determine required amount of padding
                var tail = databytes.length % 64
                // no room for msg length?
                if (tail > 56) {
                    // pad to next 512 bit block
                    for (var i = 0; i < (64 - tail); i++) {
                        databytes.push(0x0)
                    }
                    tail = databytes.length % 64
                }
                for (i = 0; i < (56 - tail); i++) {
                    databytes.push(0x0)
                }
                // message length in bits mod 512 should now be 448
                // append 64 bit, little-endian original msg length (in *bits*!)
                databytes = databytes.concat(int64_to_bytes(org_len * 8))

                // initialize 4x32 bit state
                var h0 = 0x67452301
                var h1 = 0xEFCDAB89
                var h2 = 0x98BADCFE
                var h3 = 0x10325476

                // temp buffers
                var a = 0, b = 0, c = 0, d = 0

                // Digest message
                for (i = 0; i < databytes.length / 64; i++) {
                    // initialize run
                    a = h0
                    b = h1
                    c = h2
                    d = h3

                    var ptr = i * 64

                    // do 64 runs
                    updateRun(fF(b, c, d), 0xd76aa478, bytes_to_int32(databytes, ptr),
                        7)
                    updateRun(fF(b, c, d), 0xe8c7b756, bytes_to_int32(databytes, ptr
                        + 4), 12)
                    updateRun(fF(b, c, d), 0x242070db, bytes_to_int32(databytes, ptr
                        + 8), 17)
                    updateRun(fF(b, c, d), 0xc1bdceee, bytes_to_int32(databytes, ptr
                        + 12), 22)
                    updateRun(fF(b, c, d), 0xf57c0faf, bytes_to_int32(databytes, ptr
                        + 16), 7)
                    updateRun(fF(b, c, d), 0x4787c62a, bytes_to_int32(databytes, ptr
                        + 20), 12)
                    updateRun(fF(b, c, d), 0xa8304613, bytes_to_int32(databytes, ptr
                        + 24), 17)
                    updateRun(fF(b, c, d), 0xfd469501, bytes_to_int32(databytes, ptr
                        + 28), 22)
                    updateRun(fF(b, c, d), 0x698098d8, bytes_to_int32(databytes, ptr
                        + 32), 7)
                    updateRun(fF(b, c, d), 0x8b44f7af, bytes_to_int32(databytes, ptr
                        + 36), 12)
                    updateRun(fF(b, c, d), 0xffff5bb1, bytes_to_int32(databytes, ptr
                        + 40), 17)
                    updateRun(fF(b, c, d), 0x895cd7be, bytes_to_int32(databytes, ptr
                        + 44), 22)
                    updateRun(fF(b, c, d), 0x6b901122, bytes_to_int32(databytes, ptr
                        + 48), 7)
                    updateRun(fF(b, c, d), 0xfd987193, bytes_to_int32(databytes, ptr
                        + 52), 12)
                    updateRun(fF(b, c, d), 0xa679438e, bytes_to_int32(databytes, ptr
                        + 56), 17)
                    updateRun(fF(b, c, d), 0x49b40821, bytes_to_int32(databytes, ptr
                        + 60), 22)
                    updateRun(fG(b, c, d), 0xf61e2562, bytes_to_int32(databytes, ptr
                        + 4), 5)
                    updateRun(fG(b, c, d), 0xc040b340, bytes_to_int32(databytes, ptr
                        + 24), 9)
                    updateRun(fG(b, c, d), 0x265e5a51, bytes_to_int32(databytes, ptr
                        + 44), 14)
                    updateRun(fG(b, c, d), 0xe9b6c7aa, bytes_to_int32(databytes, ptr),
                        20)
                    updateRun(fG(b, c, d), 0xd62f105d, bytes_to_int32(databytes, ptr
                        + 20), 5)
                    updateRun(fG(b, c, d), 0x2441453, bytes_to_int32(databytes, ptr
                        + 40), 9)
                    updateRun(fG(b, c, d), 0xd8a1e681, bytes_to_int32(databytes, ptr
                        + 60), 14)
                    updateRun(fG(b, c, d), 0xe7d3fbc8, bytes_to_int32(databytes, ptr
                        + 16), 20)
                    updateRun(fG(b, c, d), 0x21e1cde6, bytes_to_int32(databytes, ptr
                        + 36), 5)
                    updateRun(fG(b, c, d), 0xc33707d6, bytes_to_int32(databytes, ptr
                        + 56), 9)
                    updateRun(fG(b, c, d), 0xf4d50d87, bytes_to_int32(databytes, ptr
                        + 12), 14)
                    updateRun(fG(b, c, d), 0x455a14ed, bytes_to_int32(databytes, ptr
                        + 32), 20)
                    updateRun(fG(b, c, d), 0xa9e3e905, bytes_to_int32(databytes, ptr
                        + 52), 5)
                    updateRun(fG(b, c, d), 0xfcefa3f8, bytes_to_int32(databytes, ptr
                        + 8), 9)
                    updateRun(fG(b, c, d), 0x676f02d9, bytes_to_int32(databytes, ptr
                        + 28), 14)
                    updateRun(fG(b, c, d), 0x8d2a4c8a, bytes_to_int32(databytes, ptr
                        + 48), 20)
                    updateRun(fH(b, c, d), 0xfffa3942, bytes_to_int32(databytes, ptr
                        + 20), 4)
                    updateRun(fH(b, c, d), 0x8771f681, bytes_to_int32(databytes, ptr
                        + 32), 11)
                    updateRun(fH(b, c, d), 0x6d9d6122, bytes_to_int32(databytes, ptr
                        + 44), 16)
                    updateRun(fH(b, c, d), 0xfde5380c, bytes_to_int32(databytes, ptr
                        + 56), 23)
                    updateRun(fH(b, c, d), 0xa4beea44, bytes_to_int32(databytes, ptr
                        + 4), 4)
                    updateRun(fH(b, c, d), 0x4bdecfa9, bytes_to_int32(databytes, ptr
                        + 16), 11)
                    updateRun(fH(b, c, d), 0xf6bb4b60, bytes_to_int32(databytes, ptr
                        + 28), 16)
                    updateRun(fH(b, c, d), 0xbebfbc70, bytes_to_int32(databytes, ptr
                        + 40), 23)
                    updateRun(fH(b, c, d), 0x289b7ec6, bytes_to_int32(databytes, ptr
                        + 52), 4)
                    updateRun(fH(b, c, d), 0xeaa127fa, bytes_to_int32(databytes, ptr),
                        11)
                    updateRun(fH(b, c, d), 0xd4ef3085, bytes_to_int32(databytes, ptr
                        + 12), 16)
                    updateRun(fH(b, c, d), 0x4881d05, bytes_to_int32(databytes, ptr
                        + 24), 23)
                    updateRun(fH(b, c, d), 0xd9d4d039, bytes_to_int32(databytes, ptr
                        + 36), 4)
                    updateRun(fH(b, c, d), 0xe6db99e5, bytes_to_int32(databytes, ptr
                        + 48), 11)
                    updateRun(fH(b, c, d), 0x1fa27cf8, bytes_to_int32(databytes, ptr
                        + 60), 16)
                    updateRun(fH(b, c, d), 0xc4ac5665, bytes_to_int32(databytes, ptr
                        + 8), 23)
                    updateRun(fI(b, c, d), 0xf4292244, bytes_to_int32(databytes, ptr),
                        6)
                    updateRun(fI(b, c, d), 0x432aff97, bytes_to_int32(databytes, ptr
                        + 28), 10)
                    updateRun(fI(b, c, d), 0xab9423a7, bytes_to_int32(databytes, ptr
                        + 56), 15)
                    updateRun(fI(b, c, d), 0xfc93a039, bytes_to_int32(databytes, ptr
                        + 20), 21)
                    updateRun(fI(b, c, d), 0x655b59c3, bytes_to_int32(databytes, ptr
                        + 48), 6)
                    updateRun(fI(b, c, d), 0x8f0ccc92, bytes_to_int32(databytes, ptr
                        + 12), 10)
                    updateRun(fI(b, c, d), 0xffeff47d, bytes_to_int32(databytes, ptr
                        + 40), 15)
                    updateRun(fI(b, c, d), 0x85845dd1, bytes_to_int32(databytes, ptr
                        + 4), 21)
                    updateRun(fI(b, c, d), 0x6fa87e4f, bytes_to_int32(databytes, ptr
                        + 32), 6)
                    updateRun(fI(b, c, d), 0xfe2ce6e0, bytes_to_int32(databytes, ptr
                        + 60), 10)
                    updateRun(fI(b, c, d), 0xa3014314, bytes_to_int32(databytes, ptr
                        + 24), 15)
                    updateRun(fI(b, c, d), 0x4e0811a1, bytes_to_int32(databytes, ptr
                        + 52), 21)
                    updateRun(fI(b, c, d), 0xf7537e82, bytes_to_int32(databytes, ptr
                        + 16), 6)
                    updateRun(fI(b, c, d), 0xbd3af235, bytes_to_int32(databytes, ptr
                        + 44), 10)
                    updateRun(fI(b, c, d), 0x2ad7d2bb, bytes_to_int32(databytes, ptr
                        + 8), 15)
                    updateRun(fI(b, c, d), 0xeb86d391, bytes_to_int32(databytes, ptr
                        + 36), 21)

                    // update buffers
                    h0 = _add(h0, a)
                    h1 = _add(h1, b)
                    h2 = _add(h2, c)
                    h3 = _add(h3, d)
                }
                // Done! Convert buffers to 128 bit (LE)
                return int128le_to_hex(h3, h2, h1, h0).toUpperCase()
            }

        }

        return md5;
    })()

})(fl);


/**
 * APICloud 端API api对象 一些默认初始化设置
 */
(function ($) {
    $.setting = {
        //打开win窗口默认参数
        openWin: function () {
            this.bounces = false;
            this.bgColor = APP.bgColor;
            this.scrollToTop = false;             // （可选项）当点击状态栏，页面是否滚动到顶部。若当前屏幕上不止一个页面的scrollToTop属性为true，则所有的都不会起作用。只iOS有效
            this.vScrollBarEnabled = false;                //（可选项）是否显示垂直滚动条，布尔型，默认值：false
            this.hScrollBarEnabled = false;               //（可选项）是否显示水平滚动条，布尔型，默认值：false
            this.scaleEnabled = false;                     //（可选项）页面是否可以缩放，为true时softInputMode参数无效，布尔型，默认值：false
            this.slidBackEnabled = false; //（可选项）是否支持滑动返回。iOS7.0及以上系统中，在新打开的页面中向右滑动，可以返回到上一个页面，该字段只iOS有效
            this.slidBackType = false; //描述：（可选项）当支持滑动返回时，设置手指在页面右滑的有效作用区域。取值范围（full:整个页面范围都可以右滑返回，edge:在页面左边缘右滑才可以返回），该字段只iOS有效
            this.animation = {
                type: "movein",                //动画类型（详见动画类型常量）
                subType: "from_right",       //动画子类型（详见动画子类型常量）
                duration: device.ios ? 300 : 250                //动画过渡时间，默认300毫秒
            };
            this.delay = 100;  //毫秒（可选项）window显示延迟时间，适用于将被打开的window中可能需要打开有耗时操作的模块时，可延迟window展示到屏幕的时间，保持UI的整体性
            this.reload = true;//（可选项）页面已经打开时，是否重新加载页面，重新加载页面后apiready方法将会被执行
            this.allowEdit = true;                       //（可选项）是否允许长按页面时弹出选择菜单
            this.softInputMode = 'resize';                   //（可选项）当键盘弹出时，输入框被盖住时，当前页面的调整方式，参考键盘弹出页面调整方式常量，只iOS有效，且当scaleEnabled参数为true时无效
            this.customRefreshHeader = '';                    //（可选项）设置使用自定义下拉刷新模块的名称，设置后可以使用api.setCustomRefreshHeaderInfo方法来使用自定义下拉刷新组件用自定义下拉刷新模块的名称，设置后可以使用api.setCustomRefreshHeaderInfo方法来使用自定义下拉刷新组件
            //this.showProgressSetting = true; //描述：（可选项）是否显示等待框，此参数即将废弃，使用progress参数代替。若传了progress参数，此参数将忽略
            // this.progress={
            //    type: "",                //加载进度效果类型，默认值为default，取值范围为default|page，default等同于showProgress参数效果；为page时，进度效果为仿浏览器类型，固定在页面的顶部
            //    title: "",               //type为default时显示的加载框标题
            //    text: "",                //type为default时显示的加载框内容
            //    color: ""                //type为page时进度条的颜色，默认值为#45C01A，支持#FFF，#FFFFFF，rgb(255,255,255)，rgba(255,255,255,1.0)等格式
            //};
        },
        //打开frame窗口默认参数
        openFrame: function () {
            this.rect = {
                x: 0,             //左上角x坐标
                y: 0,             //左上角y坐标
                w: 'auto',           //宽度，若传'auto'，页面从x位置开始自动充满父页面宽度
                h: 'auto',            //高度，若传'auto'，页面从y位置开始自动充满父页面高度
                marginLeft: 0,    //相对父window左外边距的距离
                marginTop: 0,    //相对父window上外边距的距离
                marginBottom: 0,    //相对父window下外边距的距离
                marginRight: 0    //相对父window右外边距的距离
            };
            this.bounces = false;
            this.bgColor = APP.bgColor;
            this.scrollToTop = false;             // （可选项）当点击状态栏，页面是否滚动到顶部。若当前屏幕上不止一个页面的scrollToTop属性为true，则所有的都不会起作用。只iOS有效
            this.vScrollBarEnabled = false;                 //（可选项）是否显示垂直滚动条，布尔型，默认值：false
            this.hScrollBarEnabled = false;                //（可选项）是否显示水平滚动条，布尔型，默认值：false
            this.scaleEnabled = false;                      //（可选项）页面是否可以缩放，为true时softInputMode参数无效，布尔型，默认值：false
            this.animation = {
                type: "movein",                //动画类型（详见动画类型常量）
                subType: "from_right",       //动画子类型（详见动画子类型常量）
                duration: device.ios ? 300 : 250        //动画过渡时间，默认300毫秒
            };
            this.reload = true; //（可选项）页面已经打开时，是否重新加载页面，重新加载页面后apiready方法将会被执行
            this.allowEdit = true;                        //（可选项）是否允许长按页面时弹出选择菜单
            this.softInputMode = 'resize';                    //（可选项）当键盘弹出时，输入框被盖住时，当前页面的调整方式，参考键盘弹出页面调整方式常量，只iOS有效，且当scaleEnabled参数为true时无效
            this.customRefreshHeader = '';
            //this.showProgressSetting = true; //描述：（可选项）是否显示等待框，此参数即将废弃，使用progress参数代替。若传了progress参数，此参数将忽略
            // this.progress={
            //    type: "",                //加载进度效果类型，默认值为default，取值范围为default|page，default等同于showProgress参数效果；为page时，进度效果为仿浏览器类型，固定在页面的顶部
            //    title: "",               //type为default时显示的加载框标题
            //    text: "",                //type为default时显示的加载框内容
            //    color: ""                //type为page时进度条的颜色，默认值为#45C01A，支持#FFF，#FFFFFF，rgb(255,255,255)，rgba(255,255,255,1.0)等格式
            //};
        },
        //打开frameGroup窗口默认参数
        openFrameGroup: function () {
            this.bounces = false;
            this.bgColor = APP.bgColor;
            this.scrollToTop = false;             // （可选项）当点击状态栏，页面是否滚动到顶部。若当前屏幕上不止一个页面的scrollToTop属性为true，则所有的都不会起作用。只iOS有效
            this.vScrollBarEnabled = false;                 //（可选项）是否显示垂直滚动条，布尔型，默认值：false
            this.hScrollBarEnabled = false;                //（可选项）是否显示水平滚动条，布尔型，默认值：false
            this.scaleEnabled = true;                      //（可选项）页面是否可以缩放，布尔型，默认值：false
            this.allowEdit = true;                        //（可选项）是否允许长按页面时弹出选择菜单
            this.softInputMode = 'resize';                    //（可选项）当键盘弹出时，输入框被盖住时，当前页面的调整方式，参考键盘弹出页面调整方式常量，只iOS有效，且当scaleEnabled参数为true时无效
            this.customRefreshHeader = '';                    //（可选项）设置使用自定义下拉刷新模块的名称，设置后可以使用api.setCustomRefreshHeaderInfo方法来使用自定义下拉刷新组件用自定义下拉刷新模块的名称，设置后可以使用api.setCustomRefreshHeaderInfo方法来使用自定义下拉刷新组件

        },
        // ajax 默认设置
        ajax: function () {
            this.method = 'get';
            this.cache = true;
            this.timeout = AJAX.timeout;
            this.dataType = 'text';
            this.success = function (ret) {
                console.log($.log + JSON.stringify(ret))
            };
            this.fail = function (err) {
                $.toast('请求错误,代码' + err.code)
            };
        },
        // showProgressSetting
        showProgress: function () {
            this.style = 'default';
            this.animationType = 'fade';
            this.title = '加载中';
            this.text = '';
            this.modal = false;
        },
        // 网络断开提示文本
        netWorkToast: '网络已断开,请检查你的网络设置',
        // toast默认时间
        toastDuration: 2000
    };
    //返回初始化的变量
    $.getInitSetting = function (Obj) {
        return _.toPlainObject(new Obj)
    }

})(fl);


/**
 * APICloud apiready init
 */
(function ($, window) {


    /**
     * 网络状态通知给PageView
     */
    $.setPageViewNetworkStatus = function (networkType) {
        vm.$refs.loading && (vm.$refs.loading.network = networkType);
    };

    /**
     * 页面每次 apiready 默认的初始化事件
     */
    function apireadyInit() {

        /**
         * 初始化加载是否有网络，无网络就停止加载
         */
        if (vm.$refs.loading && !$.getNetwork()) {
            vm.$refs.loading.network = false;
            return;
        }

        /**
         * 开始加载页面
         */
        $.showProgress();


        /**
         *  offline 断开网络连接通知
         */
        $.apiEvent('offline', function (ret) {
            $.connectionType = false;
            $.setPageViewNetworkStatus(false);
        });

        /**
         *  online 网络在线通知
         */
        $.apiEvent('online', function (ret) {
            var networkType = ret.connectionType;
            $.connectionType = networkType;
            $.setPageViewNetworkStatus(networkType);
        });

        /**
         * 监听隐藏showProcess
         */
        $.receiveFire('hideProcess', function () {
            $.hideProgress();
        });

        /**
         * 默认页面加载完成后，引擎会对 dom 里面的元素进行 tapmode 属性解析，若是之后用代码创建的 dom 元素，则需要调用该方法后 tapmode 属性才会生效
         */
        api.parseTapmode();

        /**
         * 侵入式效果时，要修改ios样式
         */
        device.ios && api.setStatusBarStyle({
            style: 'dark'
        });

    }

    /**
     *  页面中用户未调用 apiready 的话,将手动初始化一次apiready
     */
    window.apiready = function () {
        apireadyInit();
    };

    /**
     * apiready
     *
     * @param {Function} apireadyCb
     */
    $.apiready = function (apireadyCb) {
        apiready = function () {
            apireadyInit();
            apireadyCb();
        }
    }

})(fl, window);


/**
 * APICloud 窗口
 */
(function ($) {

    /**
     * 适用于 openWin / openFrame / openFrameGroup ,传递参数设定
     * @param {Array|Object} args
     * @param {Object} initSetting 默认的设定参数
     */
    $.openParam = function (args, initSetting) {
        args = _.isArray(args[0]) ? args[0] : args;
        var args1 = args[0];
        var args2 = args[1];
        var url = _.isString(args1) ? args1 : args1.url;
        var name = _.isString(args2) ? args2 : args1.name || args1.url || args1;
        var extra = _.isObject(args2) ? args2 : args[2] || args1;
        var newArgs = _.merge($.getInitSetting(initSetting), {name: name, url: url}, extra);
        !_.isObject(newArgs.animation) && newArgs.animation && delete newArgs.animation;
        newArgs = _.merge(newArgs, newArgs.subFrame ? {
            pageParam: {
                TITLE: newArgs.title || '',
                SUBFRAME: newArgs.subFrame || ''
            }
        } : {});
        return newArgs;
    };

    /**
     * api.openWin 传递动态参数，参数的类型可以是以下选项
     * 1. [Object]  // 传递一个参数对象进入，具体请参照官网, 内部处理，如不传 name ，将用  url  做为name选项
     * 2. [String]  // 传递一个url， 将用  url  做为 name 选项
     * 3. [String] [String]  // 传递一个 name 和 url 选项
     * 4. [String] [Object]  // 传递一个 url 和 name 之外的扩展参数。将用  url  做为name选项
     * 5. [String] [String] [Object]  //  传递一个 name 和 url 选项,以及非  url 和 name 之外的扩展参数
     */
    $.openWin = function () {
        var winParam = $.openParam(Array.prototype.slice.call(arguments), $.setting.openWin);
        api.openWin(winParam);
    };

    /**
     * api.openFrame 传递动态参数，参数的类型可以是以下选项
     * 1. [Object]  // 传递一个参数对象进入，具体请参照官网, 内部处理，如不传 name ，将用  url  做为name选项
     * 2. [String]  // 传递一个url， 将用  url  做为 name 选项
     * 3. [String] [String]  // 传递一个 name 和 url 选项
     * 4. [String] [Object]  // 传递一个 url 和 name 之外的扩展参数。将用  url  做为name选项
     * 5. [String] [String] [Object]  //  传递一个 name 和 url 选项,以及非  url 和 name 之外的扩展参数
     */
    $.openFrame = function () {
        var frameParam = $.openParam(Array.prototype.slice.call(arguments), $.setting.openFrame);
        api.openFrame(frameParam);
    };

    /**
     * 在当前窗口打开Frame, 参照 openFrame。只是修改了 react 的 Y  为全局设定的间距
     */
    $.openFrameInWin = function () {
        var openFrameInWinParam = $.openParam(Array.prototype.slice.call(arguments), $.setting.openFrame);
        api.openFrame(_.assign(openFrameInWinParam, {rect: {y: $.rectY}}));
    };

    /**
     * 关闭win,默认关闭当前win
     *
     * @param {String|Array} arguments[0] '关闭指定的win或一组win'
     */
    $.closeWin = function () {
        var winNameVal = arguments[0];
        if (_.isArray(winNameVal)) {
            for (var i = 0; i < winNameVal.length; i++) {
                api.closeWin({
                    name: winNameVal[i]
                });
            }
            return;
        }
        if (_.isString(winNameVal)) {
            api.closeWin({
                name: winNameVal
            });
            return;
        }
        api.closeWin();
    };

    /**
     * 关闭frame,默认关闭当前frame
     *
     * @param {String|Array} arguments[0] '关闭指定的fame或一组frame'
     */
    $.closeFrame = function () {
        var frameNameVal = arguments[0];
        if (_.isArray(frameNameVal)) {
            for (var i = 0; i < frameNameVal.length; i++) {
                api.closeFrame({
                    name: frameNameVal[i]
                });
            }
            return;
        }
        if (_.isString(frameNameVal)) {
            api.closeFrame({
                name: frameNameVal
            });
            return;
        }
        api.closeFrame();
    };

    /**
     * 关闭应用,默认动画采用的是默认集成在原生中
     *
     * @param {Object} arguments[0] '动画设定'
     */
    $.closeWidget = function () {
        api.closeWidget(_.assign({
            silent: true,
            animation: {
                type: 'flip',
                subType: 'from_right',
                duration: 500
            }
        }, arguments || null));
    };


    /**
     * 隐藏当前frame
     *
     * @param {String} arguments[0] '默认当前frame'
     */
    $.hideFrame = function () {
        var frameName = arguments[0] || api.frameName;
        api.setFrameAttr({
            name: arguments[0] || frameName,
            hidden: true
        });
    };

    /**
     * 显示frame
     *
     * @param {String} arguments[0] '默认当前frame'
     */
    $.showFrame = function () {
        api.setFrameAttr({
            name: arguments[0] || api.frameName,
            hidden: false
        });
    };

    /**
     * 显示当前组group
     *
     * @param {String} groupName
     */
    $.showFrameGroup = function (groupName) {
        setTimeout(function () {
            api.setFrameGroupAttr({
                name: groupName,
                hidden: false
            });
        }, 500)
    };

    /**
     * 页面 loaded 手动触发完全显示
     * NOTE!! 只有在页面引用了  page-view 组件此方法才有效.
     *
     * @param {Function | Number } arguments[0] '显示之后的successCb | frame页面延迟多久显示,默认 200ms'
     * @param {Function} arguments[1] '显示之后的successCb ,只要当第一个参数为 Number时，才有效'
     */
    $.frameLoaded = function () {
        var arg = arguments[0];
        var arg1 = arguments[1];
        var setTime = _.isNumber(arg) ? arg : 200;
        var callback = ( _.isFunction(arg) || _.isFunction(arg1)) ? _.isFunction(arg) ? arg : arg1 : $.noop;
        setTimeout(function () {
            vm.$refs.loading.loading = false;
            $.hideProgress();
            callback();
        }, setTime)
    };

    /**
     * 在win窗口中 打开远程frame类型 url
     */
    $.openFrameRemotelyInWin = function () {
        $.openFrame(_.merge($.openParam(Array.prototype.slice.call(arguments), $.setting.openFrame), {
            rect: {
                y: $.rectY
            },
            animation: 'none',
            bounces: false,
            progress: {
                type: "default",                //加载进度效果类型，默认值为default，取值范围为default|page，default等同于showProgress参数效果；为page时，进度效果为仿浏览器类型，固定在页面的顶部
                title: '请稍后',
                text: '努力加载中'
            }
        }));
    }

})(fl);


/**
 * APICloud UI 组件
 */
(function ($) {

    /**
     * api.toast 位置在中间
     * @param {String} title
     * @param {Number} arguments[1] 'toastShowTime,默认 $.toastDuration'
     */
    $.toastCenter = function (title) {
        api.toast({
            msg: title,
            duration: arguments[1] || $.setting.toastDuration,
            location: 'middle'
        });
    };

    /**
     * api.toast 位置在底部
     * @param {String} title
     * @param {Number} arguments[1] 'toastShowTime,默认 $.toastDuration'
     */
    $.toast = function (title) {
        api.toast({
            msg: title,
            duration: arguments[1] || $.setting.toastDuration,
            location: 'bottom'
        });
    };

    /**
     * api.showProgress
     * @param {Object} arguments[0] '自定义参数'
     */
    $.showProgress = function () {
        api.showProgress(_.merge($.getInitSetting($.setting.showProgress), arguments[0]));
    };

    /**
     * api.hideProgress
     */
    $.hideProgress = function () {
        api.hideProgress()
    };

    /**
     * 通知隐藏页面的hideProcess
     */
    $.fireHideProcess = function () {
        $.requestFire('hideProcess');
    }

})(fl);


/**
 * APICloud Ajax
 */
(function ($) {

    var cdn; // 请求ajax的cdn
    var interFaceName; // 请求ajax的 接口名称
    var userParam;

    /**
     * 适用于ajax请求 将用户请求的参数和必须的 mid 和 mkey进行合并，传递接口
     *
     * @param {Object} param
     */
    function getAssignAjaxData(param) {
        return JSON.stringify(_.assign(param, {mid: $.mid}))
    }

    /**
     * 对ajax的动态参数设定
     *
     * @param {Array|Object} args
     */
    function ajaxParam(args) {
        var ajaxSetting = $.getInitSetting($.setting.ajax);
        var args1 = args[0];
        var args2 = args[1];
        var args3 = args[2];
        var method = args[0].method || ajaxSetting.method;
        cdn = arguments[1] || AJAX.cdn.default;
        interFaceName = _.isString(args1) ? args1 : args1.url;
        userParam = !_.isFunction(args2) ? args2 : args1.data || {};
        var fnSuccess = _.isFunction(args2) ? args2 : args3 || ajaxSetting.success;
        var fnFail = (_.isFunction(args2) && _.isFunction(args3)) ? args3 : args[3] || false;
        var data = _.eq(method, 'get') ? {
            values: {
                sign: $.md5.doEncode(getAssignAjaxData(userParam) + $.mkey),
                reqData: $.base64.doEncode(getAssignAjaxData(userParam))
            }
        } : {
            body: '\"' + $.base64.doEncode(getAssignAjaxData(_.assign(userParam, {mkey: $.mkey}))) + '\"'
        };
        var ajaxOption = _.merge(ajaxSetting, {
            url: cdn + interFaceName,
            method: method,
            data: data,
            success: fnSuccess,
            fail: fnFail
        });
        ajaxOption = _.merge({tag: ajaxOption.tag || interFaceName}, ajaxOption);
        return ajaxOption;
    }

    /**
     * api.ajax
     * @param  {Object|Array} ajaxConf 自定义的动态ajax参数，具体字段请参照官网
     * @link http://docs.apicloud.com/%E7%AB%AFAPI/api#3
     */
    $.ajax = function (ajaxConf) {
        ajaxConf = _.isObject(ajaxConf) ? _.merge($.getInitSetting($.setting.ajax), ajaxConf) : ajaxParam(Array.prototype.slice.call(arguments));
        var ajax_startTime = new Date().getTime();
        api.ajax(ajaxConf, function (ret, err) {
            var retResult = ret;
            ret && (ret = JSON.parse($.base64.doDecode(ret)));
            if (DEBUG) {
                /**
                 * 打印日志信息
                 */
                console.group();
                console.warn($.log + 'Start_接口：' + interFaceName + '===>数据大小：( ' + $.getStringByte(retResult) + ' ) ' + $.log + '耗时：( ' + (new Date().getTime() - ajax_startTime) + 'ms )    =======================================');
                console.warn("cdn：" + cdn);
                console.warn("url：" + cdn + interFaceName);
                console.warn("tag：" + ajaxConf.tag);
                console.warn("method：" + ajaxConf.method);
                console.warn("custom param：" + JSON.stringify(userParam));
                if (ajaxConf.method == 'get') {
                    console.warn("data source：" + 'reqData=' + getAssignAjaxData(userParam) + '&&sign=' + (getAssignAjaxData(userParam) + $.mkey));
                    console.warn("merge data reqData:" + $.base64.doEncode(getAssignAjaxData(userParam)));
                    console.warn("merge data sign:" + $.md5.doEncode(getAssignAjaxData(userParam) + $.mkey));
                }
                else {
                    console.warn("data source：" + getAssignAjaxData(_.assign(userParam, {mkey: $.mkey})));
                    console.warn("merge data body：" + '\"' + $.base64.doEncode(getAssignAjaxData(_.assign(userParam, {mkey: $.mkey}))) + '\"');
                }
                console.warn("timeout：" + ajaxConf.timeout);
                console.warn("dataType：" + ajaxConf.dataType);
                console.warn("result：" + JSON.stringify(ret));
                console.warn("result data：" + JSON.stringify(ret.data));
                console.warn("code：" + ret.code);
                console.warn("msg：" + ret.msg);
                console.warn($.log + 'End_接口：' + interFaceName + '   =======================================');
                console.groupEnd();
            }
            if (ret) {
                if (ret.code != '10000') {
                    $.hideProgress();
                    $.toastCenter('登陆已失效,请重新登陆');
                    return false;
                }
                ajaxConf.success(ret);
            }
            else {
                //0  连接错误
                //1  超时
                //2  授权错误
                //3  数据类型错误
                $.hideProgress();
                var errorCode = err.code;
                if (errorCode < 2 && !ajaxConf.fail) {
                    console.warn(errorCode);
                    $.toast($.setting.netWorkToast);
                    return;
                }
                ajaxConf.fail && ajaxConf.fail(err);
            }
        })
    };

    /**
     * 采用 api.ajax 默认 post 方法
     *
     * @param {String |Object}  interFaceName | Object '接口名称/全部自定义参数'
     * @param {Object| Function} Object | successCb '接口是否接收参数,如无，直接 successCb 接口' (第一个参数 Object，此处可忽略)
     * @param {Function}  successCb '如接口无参数传递，此参数可忽略'  (第一个参数 Object，此处可忽略)
     */
    $.post = function () {
        var postAjaxConf = _.assign(ajaxParam(Array.prototype.slice.call(arguments), AJAX.cdn.order), {method: 'post'});
        $.ajax(postAjaxConf)
    };

    /**
     * 采用 api.ajax 默认 get 方法
     *
     * @param {String |Object}  interFaceName | Object '接口名称/全部自定义参数'
     * @param {Object| Function} Object | successCb '接口是否接收参数,如无，直接 successCb 接口' (第一个参数 Object，此处可忽略)
     * @param {Function}  successCb '如接口无参数传递，此参数可忽略'  (第一个参数 Object，此处可忽略)
     */
    $.get = function () {
        var getAjaxConf = ajaxParam(Array.prototype.slice.call(arguments));
        $.ajax(getAjaxConf);
    };

})(fl);


/**
 * APICloud 事件 ---监听
 */
(function ($) {

    /**
     * api.sendEvent 自定义事件
     *
     * @param {String} eventsName
     * @param {Object} arguments[1]
     */
    $.requestFire = function (eventsName) {
        api.sendEvent({
            name: eventsName,
            extra: arguments[1] ? arguments[1] : ''
        });
    };

    /**
     * api.addEventListener 自定义事件
     *
     * @param {String} eventsName
     * @param {Function} successCb(ret.value)
     * @param {Function} errorCb(err)
     */
    $.receiveFire = function (eventsName, successCb) {
        api.addEventListener({
            name: eventsName
        }, function (ret, err) {
            if (ret) {
                successCb(ret.value);
            }
            else {
                arguments[2] && arguments[2](err);
            }
        })
    };

    /**
     * api.addEventListener 原生事件
     *
     * @param {String} eventsName
     * @param {Function} successCb(ret)
     * @param {Function} errorCb(err)
     */
    $.apiEvent = function (eventsName, successCb) {
        api.addEventListener({
            name: eventsName
        }, function (ret, err) {
            if (ret) {
                successCb(ret);
            }
            else {
                arguments[2] && arguments[2](err)
            }
        })
    };


})(fl);


/**
 * APICloud 原生事件  --- keyback
 */
(function ($, window) {

    /**
     * 全局物理返回按键事件默认操作 api.closeWin()
     */
    window.keyBack = function () {
        $.triggerKeyBack();
    };

    /**
     * 监听物理返回按键事件
     * @param callback
     */
    $.keyBackEvent = function (callback) {
        $.receiveFire('keyback', function (param) {
            callback(param);
        })
    };

    /**
     * 监听自定义的返回事件。可用在 frame 或 win页面
     * NOTE!! 如果用在 frame 页面，请在 win 窗口禁用 keyBackDisable 返回键操作.要不失效
     * @param {String|Function} function
     * 1. 如果是 win 窗口调用，参数必须是 Fun 类型,就不需要传递第二个参数。
     * 2. 如果是 frame 窗口调用
     *    可以是 String 类型，指定的fameName,例如在 win 窗口有多个frame的特殊情况
     *    如果是 Fun 类型，,就不需要传递第二个参数。
     * @param {Function} function '适用于调用的窗口类型为 frame,且第一个参数是 String 类型的 frameName'
     */
    $.keyBack = function (keyBack) {
        var $winName = api.winName;
        var $frameName = api.frameName;
        var keyBackHandle = _.isFunction(keyBack) ? keyBack : arguments[1],
            frameName = _.isString(keyBack) ? keyBack : $frameName;
        if ($frameName) {
            window.keyBack = keyBackHandle;
            $.keyBackFrame = frameName;
            $.requestFire('keyBack-' + $winName, {
                frameName: frameName
            });
        }
        else {
            $.keyBackWin = true;
            window.keyBack = keyBack;
            $.keyBackEvent(function () {
                api.execScript({
                    name: $winName,
                    script: 'keyBack()'
                });
            })
        }
    };

    /**
     * 禁用 win 窗口的返回按钮，改由 frame 来响应
     */
    $.keyBackDisable = function () {
        // 响应接收来自调用 frame 窗口返回键的操作
        // 如果frame窗口未调用 keyBack 的话,win 窗口的禁用 back 返回键将失效，还是会采取默认的返回操作
        $.receiveFire('keyBack-' + api.winName, function (param) {
            $.keyBackFrame = param.frameName;
            $.keyBackEvent(function () {
                api.execScript({
                    frameName: $.keyBackFrame,
                    script: 'keyBack()'
                })
            })
        });
    };

    /**
     * 模拟响应手机返回键的操作,主要用到头部的返回操作
     */
    $.triggerKeyBack = function () {
        // 如果当前 keyBackFrame 不为空的话，就响应 win 窗口的返回按键操作
        if ($.keyBackFrame != false && $.keyBackWin == false) {
            api.execScript({
                frameName: $.keyBackFrame,
                script: 'keyBack()'
            });
            return;
        }
        // 如果当前 keyBackWin 不为空的话，就响应 win 窗口的返回按键操作
        if ($.keyBackWin != false) {
            api.execScript({
                name: api.winName,
                script: 'keyBack()'
            });
            return;
        }
        // frame 和 win 都木有调用返回按键的话，就默认关闭当前 win 窗口
        api.closeWin();
    };

})(fl, window);


/**
 *  APICloud storage
 */
(function ($) {
    /**
     * 针对缓存的数组类型操作
     * 1.可以读写操作,可以限制写入的最大长度
     * 2.删/写同时操作
     * @mark: 如需要返回最新结果，第二个参数加一个回调函数即可
     * @param option
     * {
     *    key: {String},  -- storage key
     *    write: {String | Object} -- 写入的缓存数据
     *    max: {Number} 最大长度  -- 最大长度
     *    del: {String | Object} -- 对应写入的缓存数据类型
     * }
     */
    $.storageArray = function (option) {
        var key = option.key;  // storage key
        var newStorage = option.write || false;  // 是否需要添加的storage
        var delStorage = option.del || false; // {} '' ,  是否删除旧的信息
        var maxLength = option.max || false;   // 该key最大存储多少长度
        var delDouble = delStorage || newStorage; // 删除重复的
        var resourceStorage = $api.getStorage(key) || [];
        var resourceStorageLength = resourceStorage.length;
        // 最大长度限制,删除第一个添加的历史纪录
        if (maxLength && resourceStorageLength > maxLength - 1 && !delStorage) {
            resourceStorage = _.drop(resourceStorage);
        }
        // 删除指定数组中符合的值/添加的时候，如果存在完全符合的话，就会删除原来要添加的那个值的位置，类型是 ['','','']
        if (_.isString(delStorage) || newStorage) {
            resourceStorage = _.pull(resourceStorage, delDouble);
        }
        // 删除指定数组中符合的值，类型是 [{},{},{}] 传递公共对象所属的唯一性标识
        if (_.isObject(delStorage) || newStorage) {
            resourceStorage = _.dropRightWhile(resourceStorage, delDouble);
        }
        // 添加操作
        newStorage && resourceStorage.push(newStorage);
        $api.setStorage(key, resourceStorage);
        arguments[1] && arguments[1]($api.getStorage(key));
    }
})(fl);


/**
 * APICloud constant
 */
(function ($) {

    /**
     * 获取网络状态，如果为  none 就返回false 否则就返回对应的网络类型
     * @returns {Boolean|String}
     */
    $.getNetwork = function () {
        var connectionType = api.connectionType;
        if (connectionType == 'none') {
            return false;
        }
        return connectionType;
    };

})(fl);


/**
 * 其他
 */
(function ($, window) {
    /**
     * 获取 url地址栏参数
     *
     * @param {String} paramName 'param名称'
     * @returns {null|String}
     */
    $.getQueryString = function (paramName) {
        var reg = new RegExp("(^|&)" + paramName + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return (r[2]);
        return null;
    };

    /**
     * 获取字符串的字节数
     * @param str
     * @returns {String}
     */
    $.getStringByte = function (str) {
        var bytesCount = 0;
        for (var i = 0; i < str.length; i++) {
            var c = str.charAt(i);
            // 用正则判断是不是中文，如果是的话，字节数就加1。 //匹配双字节
            if (/^[\u0000-\u00ff]$/.test(c)) {
                bytesCount += 1;
            }
            else {
                bytesCount += 2;
            }
        }
        return bytesCount + 'Byte=' + (bytesCount / 1024).toFixed(2) + 'K=' + (bytesCount / 1024 / 1024).toFixed(2) + 'M';
    }
})(fl, window);


/**
 * 与原生 native交互
 */
(function ($) {
    $.accessNative = function (name) {
        var successCb = arguments[0] || function (ret) {
            };
        api.accessNative({
            name: name,
            extra: arguments[1] || ''
        }, function (ret, err) {
            successCb(ret, err);
        })
    };


})(fl);


/**
 * modal
 */
(function ($, window) {


    /**
     * 在当前窗口打开 frameModal 的初始化参数
     * NOTE!! 如未调用此方法，直接调用 openFrameModal 将会报错
     * 可决定父窗口
     *
     */
    $.modalWinInit = function () {
        //var customParam = arguments[0] || {};
        // 当frameModal窗口关闭时,需修改win窗口的 modalFrameName 为false
        window.setModalFrameName = function () {
            vm.modalFrameName = false;
        };
        Vue.mixin({
            data: function () {
                return {
                    modalParent: true, //表明我是父窗口的操作
                    modalFrameName: false // 当前打开的fameModal名称
                }
            },
            watch: {
                modalFrameName: function (newVal, oldVal) {
                    if (!newVal) {
                        $.closeFrame(oldVal);
                        api.frameName && $.requestFire('keyBack-' + api.winName, {
                            frameName: api.frameName
                        });
                    }
                }
            }
        });
    };

    /**
     * frameModal窗口的初始化参数设定
     * 'default：{ mdoalClose:true } '
     *  NOTE!! 只有调用此方法，在本 frameNModal 调用 showFrameModal 才有效果
     * @param {Object} object
     */
    $.modalFrameInit = function () {
        var customParam = arguments[0] || {};
        Vue.mixin({
            data: function () {
                return _.merge({
                    modalClose: true,  // 初始化的时候/打开窗口动态传递，根据设定（点击模态背景===返回按键）是否关闭窗口
                    modal: false,      // frameModal窗口是否显示参数
                    parentWin: ''     // 打开此窗口的窗口类型  frame or win
                }, customParam)
            }
        })
    };

    /**
     * 打开frameModal窗口
     */
    $.openFrameModal = function () {
        $.showProgress();
        // 判定当前窗口的类型
        var winType = api.frameName ? {
            frameName: api.frameName
        } : {
            name: api.winName
        };
        var frameParam = _.merge($.openParam(Array.prototype.slice.call(arguments), $.setting.openFrame), {
            animation: 'none',
            bgColor: 'rgba(0,0,0,0)',
            // 此处将打开frameModal窗口的父窗口类型传递给frameModal纪录
            // 为了在关闭窗口时，改变父窗口的frameModal状态
            pageParam: {
                parentWin: winType
            }
        });
        /**
         * 更改打开frameModal窗口的父窗口状态
         * NOTE!! 此处依赖 frameModal窗口的父窗口 调用 modalWinInit() 方法.否则将报错
         */
        vm.modalFrameName = frameParam.name;
        vm.$nextTick(function () {
            $.openFrame(frameParam);
        });
    };

    /**
     * 关闭FrameModal窗口
     */
    $.closeFrameModal = function () {
        // 如果是在打开该frameModal的父窗口调用关闭当前frameModal
        // 就直接更改 frameModal状态
        if (vm.modalParent) {
            vm.modalFrameName = false;
            return;
        }
        // 通知打开该frameModal父窗口更改frameModal状态
        api.execScript(_.assign({
            script: 'setModalFrameName()'
        }, vm.parentWin));
    };

    /**
     * 将frameModal窗口要显示的DOM显示出来。
     * NOTE!!
     * 1.依赖被打开的frameModal需调用 modalFrameInit 方法
     * 2.需要在frameModal的 apiready 初始化后调用才方法才有效
     *
     * @param {number} frameInDomShowTime
     */
    $.showFrameModal = function () {

        var showTime = arguments[0] || 100;
        /**
         * 直接合并更新 vm $data 对象。
         * NOTE!! 请在页面初始化需要有可能从父窗口传递过来更新的对象
         */
        vm.$data = _.assign(vm.$data, api.pageParam);
        // 更新对象后，开始初始化frameModal操作
        vm.$nextTick(function () {
            /**
             * 当前frameModal是否响应返回键关闭该frameModal
             * NOTE!! 如果当前页面自己调用了 keyBack 该方法将失效.
             */
            $.keyBack(function () {
                vm.modalClose && $.closeFrameModal();
            });
            // 隐藏父窗口loading效果
            $.fireHideProcess();
            // 更改当前frame窗口背景为透明
            api.setFrameAttr({
                name: api.frameName,
                bgColor: 'rgba(0,0,0,0.4)'
            });
            // 显示在frameModal的DOM内容。为了可能防止frameMoal还未显示，页面就show出来，造成动画不流畅。
            setTimeout(function () {
                vm.modal = true;
            }, showTime);
            //点击模态背景，是否响应关闭窗口的操作，NOTE!! 根据初始化的 modalClose 设定
            document.addEventListener('click', function (e) {
                if (e.target.classList.contains('modal-container') && vm.modalClose) {
                    $.closeFrameModal();
                }
            }, false);
        });
    };


})(fl, window);


/**
 * validate regRex
 */
(function ($) {
    /**
     * 动态正则验证
     *
     * @param {String} validateVal
     * @param {Array|String}regRexAry
     * @returns {boolean}
     */
    $.regRex = function (validateVal, regRexAry) {
        regRexAry = _.isString(regRexAry) ? [regRexAry] : regRexAry;
        for (var i = 0; i < regRexAry.length; i++) {
            if (!eval(regRexAry[i]).test(validateVal)) return false;
        }
        return true;
    }
})(fl);