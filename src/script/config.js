/**
 * 设备类型和设备版本 检测
 * @link https://github.com/nolimits4web/Framework7/blob/master/src/js/proto-device.js
 * @date: on 22 Feb 2015
 */
(function (window) {
    var device = {};
    var ua = navigator.userAgent;
    var android = ua.match(/(Android);?[\s\/]+([\d.]+)?/);
    var ipad = ua.match(/(iPad).*OS\s([\d_]+)/);
    var ipod = ua.match(/(iPod)(.*OS\s([\d_]+))?/);
    var iphone = !ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/);
    device.ios = device.android = device.iphone = device.ipad = device.androidChrome = false;
    // Android
    if (android) {
        device.os = 'android';
        device.osVersion = android[2];
        device.android = true;
        device.androidChrome = ua.toLowerCase().indexOf('chrome') >= 0;
    }
    if (ipad || iphone || ipod) {
        device.os = 'ios';
        device.ios = true;
    }
    // iOS
    if (iphone && !ipod) {
        device.osVersion = iphone[2].replace(/_/g, '.');
        device.iphone = true;
    }
    if (ipad) {
        device.osVersion = ipad[2].replace(/_/g, '.');
        device.ipad = true;
    }
    if (ipod) {
        device.osVersion = ipod[3] ? ipod[3].replace(/_/g, '.') : null;
        device.iphone = true;
    }
    // iOS 8+ changed UA
    if (device.ios && device.osVersion && ua.indexOf('Version/') >= 0) {
        if (device.osVersion.split('.')[0] === '10') {
            device.osVersion = ua.toLowerCase().split('version/')[1].split(' ')[0]
        }
    }
    // Webview
    device.webView = (iphone || ipad || ipod) && ua.match(/.*AppleWebKit(?!.*Safari)/i);
    window.device = device;
})(window);


/**
 * 是否开启DEBUG模式
 * 0：关闭
 * 1：打开
 */
DEBUG = 1;

/**
 * 0：离线
 * 1：打包
 */
RELEASED = 0;
// 目录配置
wgtRootDir = RELEASED ? '/www/fuelCard' : '';


/**
 * 设计稿 Base DPR
 */
DPR = 3;


/**
 * 各种大小
 */

APP = {
    bgColor: '#eee'
};


/**
 * 头部大小
 */
HEAD = {
    height: 147,
    fontSize: 45,
    bgColor: '#f8f8f8',
    color: '#484848',
    status: {
        android: 75,
        ios: 60
    },
    title: '加油卡',
    class: device.ios ? 'ios-status' : ''
};
HEAD.heightContainStatus = {
    android: HEAD.height + HEAD.status.android,
    ios: HEAD.height + HEAD.status.ios
};


/**
 * 返回 url 地址栏参数
 * @param name
 * @returns {*}
 */
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return (r[2]);
    return null;
}


/**
 * 返回基准的 px 值
 * @param px [init]
 * @returns {string}
 */
function getThreeVersionPx(px) {
    var dpr1 = px / 3;
    var dpr2 = dpr1 * 2;
    var dpr3 = px;
    return [dpr1, dpr2, dpr3][lib.flexible.dpr - 1] + 'px';
}


/**
 *  返回不带 px 的number 值
 * @param px
 * @returns {Number}
 */
function getThreeVersion(px) {
    var dpr1 = px / 3;
    var dpr2 = dpr1 * 2;
    var dpr3 = px;
    return parseInt([dpr1, dpr2, dpr3][lib.flexible.dpr - 1]);
}


/**
 * 首页专用,获取缓存的key
 */
function rootSetStorage() {
    getQueryString('native') && $api.setStorage('storage', {
        mkey: getQueryString('mkey'),
        mid: getQueryString('mid'),
        account: getQueryString('account'),
        nickName: getQueryString('nickName')
    });
}


/**
 * 获取 widget 真实路径
 */
function getWidgetPath() {
    return api.wgtRootDir + wgtRootDir;
}



/**
 * 个性化配置====================================================
 */
/**
 * aja 配置
 */
AJAX = {
    timeout: 10,
    cdn: {
        "order": "http://120.26.208.13:8002/order/",
        "default": "http://120.26.208.13/fulu_srv/gasCardRechargeApi/"
    },
    href: {
        // 查询单个货品信息
        "getSupplyInfo": "getSupplyInfo",
        // 查询全部卡信息
        "home": "home",
        // 查询卡主信息
        "getCardInfo": "getCardInfo"
    }
};
STORAGE = {
    cardInfo: 'cardInfo'
};




