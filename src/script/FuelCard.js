
Vue.directive('link-kefu', {
    bind: function () {
        this.el.addEventListener('click', function () {
            api.accessNative({
                name: 'kefu',
                extra: {
                    orderTypeName: '加油卡'
                }
            })
        }, false)
    }
});