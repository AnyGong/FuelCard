/*
 * Fulu Pay Network Technology Co., Ltd.
 *
 * 支付插件
 * 基于 APICloud
 * Author：AnyGong
 * @since 2016.05.31
 * */
!function (factory) {
    if (typeof require === 'function' && typeof exports === 'object' && typeof module === 'object') {
        var target = module['exports'] || exports;
        factory(target);
    } else if (typeof define === 'function' && (define.amd || define.cmd)) {
        define(['exports'], factory);
    } else {
        factory(window['fuluPay'] = {});
    }
}(function ($) {


    // 设定支付插件的名称

    $.pingPay = 'pingCharge'; //interfaceCode!=10000 ping++
    $.taobaoPay = 'taobaoCharge'; //interfaceCode=10000 原生支付宝
    $.taobaoPayName = 'jiayouka'; // 支付标识

    return fuluPay;
});

(function (pay) {
    /**
     * 购买非天猫商品采取的 ping++
     * interfacecode!=10000
     *
     * @param option {Object}  // 由以下属性组成
     * {
     *    store:{Object}    // '当前购买的货品对象'  ===> 必须具有以下属性  oldGoodsID(货品id),purchasePrice(价格)
     *    payType:{init} //  // '支付类型',  0:支付宝  1:微信
     *    account:{String} // 充值账号  ===> 如：QQ号，手机号，加油卡，网游账号等
     * }
     * @param successCb {Function}  // 支付结果回调  ret.result   0: 支付失败(取消/各种原因)  1：支付成功
     * @throws console.error('调用非天猫商品的支付方式时,参数错误');
     */
    pay.buySelfGoods = function (option, successCb) {
        var store = option.store;
        if (Array.prototype.slice.call(arguments).length != 2) {
            console.error('调用非天猫商品的支付方式时,参数错误');
            return;
        }
        DEBUG && console.warn('===>buySelfGoods Success, This arguments value：' + " || payType==>" + option.payType + " || " + "chargeAccount==>" + option.account + " || " + "oldGoodsID==>" + store.oldGoodsID + " || " + "purchasePrice==>" + store.purchasePrice + ' || ');
        api.accessNative({
            name: pay.pingPay,
            extra: {
                payType: option.payType,
                chargeAccount: option.account,
                oldGoodsID: store.oldGoodsID,
                purchasePrice: store.purchasePrice
            }
        }, function (ret, err) {
            ret && DEBUG && console.warn('===>buySelfGoods Success, This Result：' + JSON.stringify(ret));
            ret && successCb(ret);
        })
    };
    /**
     * 购买天猫商品采取的原生支付宝
     * interfacecode!=10000
     *
     * @param option {Object}  // 由以下属性组成
     * {
     *    store:{Object}    // '当前购买的货品对象'  ===> 必须具有以下属性  goodsNo(淘宝id),oldGoodsID(货品id),purchasePrice(价格)
     *    account:{String} // 充值账号  ===> 如：QQ号，手机号，加油卡，网游账号等
     *    amount:{String} //  ===>对应现有的字段属性是面值的   amount
     * }
     * @param successCb {Function}  // 支付结果回调  ret.result   0: 支付失败(取消/各种原因)  1：支付成功   2:变价提示
     * @throws console.error('调用天猫商品的支付方式时,参数错误');
     */
    pay.buyTMGoods = function (option, successCb) {
        var store = option.store;
        if (Array.prototype.slice.call(arguments).length != 2) {
            console.error('调用天猫商品的支付方式时,参数错误');
            return;
        }
        DEBUG && console.warn('==>buyTMGoods Success, This arguments value：' + "|| payName==>" + pay.taobaoPayName + " || payName==>" + store.goodsNo + " || " + " || itemId==>" + store.goodsNo + " || " + "account==>" + option.account + " || " + "goodsId==>" + store.oldGoodsID + " || " + "price==>" + store.purchasePrice + " || " + "parvalue==>" + option.amount + ' || ');
        api.accessNative({
            name: pay.taobaoPay,
            extra: {
                payName: pay.taobaoPayName,
                itemId: store.goodsNo,
                account: option.account,
                goodsId: store.oldGoodsID,
                price: store.purchasePrice,
                parvalue: option.amount
            }
        }, function (ret, err) {
            ret && DEBUG && console.warn('==>buyTMGoods Success, This Result：' + JSON.stringify(ret));
            ret && successCb(ret);
        })
    };

    // 购买商品,两种类型合并
    /**
     *
     * 合并购买时，需要传递多个对象
     * @param option {Object}  // 由以下属性组成
     * {
     *    store:{Object}    // '当前购买的货品对象'  ===> 必须具有以下属性  goodsNo(淘宝id),oldGoodsID(货品id),purchasePrice(价格)
     *    payType:{init}   // '支付类型',  0:支付宝  1:微信
     *    account:{String} // 充值账号  ===> 如：QQ号，手机号，加油卡，网游账号等
     *    amount:{String} //  ===>对应现有的字段属性是面值的   amount
     * }
     * @param successCb
     */
    pay.buyGoods = function (option, successCb) {
        var store = option.store;
        var account = option.account;
        // 购买天猫商品 interfaceCode = 10000
        if (store.interfaceCode == '10000') {
            pay.buyTMGoods({
                store: store,
                account: account,
                amount: option.amount
            }, function (ret) {
                successCb(ret);
            });
        }
        // 购买自营商品 interfaceCode != 10000
        else {
            pay.buySelfGoods({
                store: store,
                account: account,
                payType: option.payType
            }, function (ret) {
                successCb(ret);
            });
        }
    };
})(fuluPay);