// 面值管理
Vue.component('parvalue-list', {
    template: '<ul class="ui-parvalue-list animated" v-show="parvalueList.length" transition="fade" v-tap-mode="parvalueList">' +
                    '<li @click="selectedParvalue=parvalue" :class="{\'selected\':selectedParvalue.id==parvalue.id}" v-for="parvalue in parvalueList">' +
                        '<div class="par-value-item">' +
                            '<p class="par-value-price" v-text="parvalue.name"></p>' +
                            '<p v-show="parvalue.discountNew" class="par-value-discount" v-text="parvalue.discountNew"></p>' +
                        '</div>' +
                    '</li>' +
                '</ul>',
    props: {
        parvalueList: {
            default: function () {
                return[]
            }
        },
        selectedParvalue: {
            twoWay: true,
            default: function () {
                return {}
            }
        }
    },
    watch:{
        selectedParvalue:function (val) {
            val && this.$dispatch('selectedParvaluehandle',val);
        }
    }
});

// 货品管理
Vue.component('paystore-list', {
    template: '<ul class="ui-store-list animated" v-show="paystoreList.length" transition="fadeStore" v-tap-mode="paystoreList">' +
                '<li track-by="$index" class="store-list-bd" v-for="store in paystoreList">' +
                    '<div class="store-item store-name-price">' +
                        '<div tapmode @click="openTagDetailModal(store.tags)" class="store-logo-name store-item-l">' +
                             '<img src="../image/placeholder-store.png" v-cache-img="getStoreTypeIcon(store.interfaceCode)" class="store-type-logo">' +
                             '<p class="store-name" v-text="store.supplierName"></p>' +
                        '</div>' +
                        '<p class="store-price store-item-r">&#165;<span v-text="store.salePrice"></span></p>' +
                    '</div>' +
                    '<div class="store-item store-other">' +
                        '<div tapmode @click="openTagDetailModal(store.tags)" class="store-item-l store-attr-list">' +
                            '<ul class="store-experience-list">' +
                                '<li v-for="experience in getExperienceList(store)" v-show="experience.val && experience.val != \'0.00\'" class="store-experience-bd" v-text="experience.title+experience.val"></li>'+
                            '</ul>' +
                            '<ul class="store-tag-list">' +
                                 '<li class="store-tag-bd" :style="getStoreTagStyle(tag.color)" v-for="tag in store.tags" v-text="tag.name"></li>' +
                            '</ul>' +
                        '</div>' +
                        '<div tapmode @click="buyGoods(store)" class="store-item-r store-buy">' +
                              '<div class="store-submit-buy">' +
                                 '<button class="store-buy-btn">购买</button>' +
                              '</div>' +
                        '</div>' +
                    '</div>' +
                '</li>' +
            '</ul>'+
            '<p v-show="storeData" class="ui-store-none">该面值暂无货品，请选择其他面值</p>',
    props: {
        interfaceList: {
            default: []
        },
        paystoreList: {
            default: function () {
                return []
            }
        }
    },
    data:function(){
      return {
          storeData:false
      }
    },
    transitions:{
        fadeStore:{
            beforeEnter: function () {
                this.paystoreList.length && (this.storeData=false);
            },
            leave: function () {
                !this.paystoreList.length && (this.storeData=true);
            },
            enterClass: 'zoomIn',
            leaveClass: 'fadeOut'
        }
    },
    methods: {
        getExperienceList:function (store) {
            return [
                {title: '福禄积分+', val: store.points},
                {title: '天猫经验+', val: store.tbExp},
                {title: '天猫积分+', val: store.tbPoints},
                {title: '充值返钱+', val: store.cashBack}
            ]
        },
        // 打开标签详情弹窗
        openTagDetailModal: function (tags) {
            tags.length && fl.openFrameModal(getWidgetPath() +'/template/tagDetailsModal.html', 'tagModal.html', {
                pageParam: {
                    tags: tags
                }
            })
        },
        // 返回设置的标签样式
        getStoreTagStyle: function (value) {
            return value ? {
                color: value,
                borderColor: value
            } : ''
        },
        //返回店铺的类型图标
        getStoreTypeIcon: function (code) {
            return _.find(this.interfaceList, {code: code.toString()}).iconUrl;
        },
        // 购买按钮
        buyGoods: function (store) {
            this.$dispatch('buyGoodsHandle', store);
        }
    }
});