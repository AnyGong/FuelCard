var _=require('lodash')

var users = [
    { 'user': 'barney',  'active': true },
    { 'user': 'fred',    'active': false },
    { 'user': 'pebbles', 'active': false }
];

// _.dropRightWhile(users, function(o) { return !o.active; });
// → objects for ['barney']

// The `_.matches` iteratee shorthand.
console.log(_.drop(users));
// → objects for ['barney', 'fred']

// The `_.matchesProperty` iteratee shorthand.
// _.dropRightWhile(users, ['active', false]);
// // → objects for ['barney']
//
// // The `_.property` iteratee shorthand.
// _.dropRightWhile(users, 'active');
// → objects for ['barney', 'fred', 'pebbles']